package applause

import (
	"context"
	"fmt"
	"reflect"
	"regexp"
	"strings"

	errör "gitlab.com/biffen/error"
	"gitlab.com/biffen/go-applause/internal"
)

type Parser struct {
	PassThrough bool

	named     map[string]*internal.Option
	patterned []struct {
		regexp.Regexp
		*internal.Option
	}
}

func (parser *Parser) Add(options ...Option) {
	for _, option := range options {
		target := reflect.ValueOf(option.Target)

		if target.Kind() == reflect.Invalid {
			panic(fmt.Sprintf("invalid target %T", option.Target))
		}

		o := internal.Option{
			Add:      option.Add,
			Names:    option.Names,
			Negate:   option.Negate,
			Patterns: option.Patterns,
			Target:   target,
		}

		parser.add(o)
	}
}

func (parser *Parser) AddStruct(s any) {
	for _, option := range internal.OptionsForStruct(s) {
		parser.add(option)
	}
}

func (parser *Parser) Parse(
	ctx context.Context,
	args []string,
) (operands []string, err error) {
	var (
		i = 0

		next = func(n string) func() (string, error) {
			return func() (string, error) {
				i++
				if i < len(args) {
					return args[i], nil
				}

				return "", fmt.Errorf("%w: %+v", ErrNoValue, n)
			}
		}
	)

OUTER:
	for ; i < len(args); i++ {
		select {
		case <-ctx.Done():
			err = ctx.Err()

			return
		default:
		}

		arg := args[i]

		switch {
		case arg == "--":
			i++

			if i < len(args) {
				operands = append(operands, args[i:]...)
			}

			return

		case strings.HasPrefix(arg, "--"):
			var (
				parts = strings.SplitN(arg[2:], "=", 2)
				value Value
			)

			name := parts[0]

			if len(parts) > 1 {
				value = NewValue(parts[1])
			} else {
				value = NewValue(next(name))
			}

			found, err := parser.set(ctx, name, value)
			if err != nil {
				return nil, err
			}

			if found {
				continue
			}

			if !parser.PassThrough {
				return nil, fmt.Errorf("%w: %s", ErrUnknownOption, formatOption(name))
			}

		case len(arg) > 1 && strings.HasPrefix(arg, "-"):
			str := arg[1:]

			for str != "" {
				name := string(str[0])

				var value Value

				if len(str) > 1 {
					if str[1] == '=' {
						// Explicit value
						if len(str) == 1+1 {
							// Explicit empty value (`-o=`)
							value = NewValue("")
						} else {
							value = NewValue(str[2:])
						}

						str = ""
					} else {
						// Implicit value; maybe the rest of the arg
						value = NewValue(func() (v string, err error) {
							v = str[1:]
							str = str[:1]

							return
						})
					}
				} else {
					value = NewValue(next(name))
				}

				found, err := parser.set(ctx, name, value)
				if err != nil {
					return nil, err
				}

				if found {
					if len(str) <= 1 {
						// No more bundled short options
						continue OUTER
					}

					// Next short option in the bundle
					str = str[1:]

					continue
				}

				if parser.PassThrough {
					// Stop processing bundle and leave the rest in `str`
					arg = "-" + str

					break
				}

				return nil, fmt.Errorf("%w: %s", ErrUnknownOption, formatOption(name))
			}
		}

		operands = append(operands, arg)
	}

	return
}

func (parser *Parser) add(o internal.Option) {
	o.Setup()

	parser.addNamed(&o)

	for _, pattern := range o.Patterns {
		parser.patterned = append(parser.patterned, struct {
			regexp.Regexp
			*internal.Option
		}{*pattern, &o})
	}

	if o.Negate {
		var (
			n internal.Option
			v = o.Target.Elem()
		)

		for _, name := range o.Names {
			if len(name) <= 1 {
				continue
			}

			n.Names = append(n.Names, "no-"+name)
		}

		switch v.Kind() {
		case reflect.Bool:
			n.NoValueFunc = func(context.Context, string) error {
				v.SetBool(false)
				return nil
			}

		default:
			n.NoValueFunc = func(context.Context, string) error {
				v.Set(reflect.New(v.Type()).Elem())
				return nil
			}
		}

		parser.addNamed(&n)
	}
}

func (parser *Parser) addNamed(o *internal.Option) {
	if parser.named == nil {
		parser.named = make(map[string]*internal.Option)
	}

	for _, name := range o.Names {
		if _, ok := parser.named[name]; ok {
			panic(fmt.Sprintf("duplicate option %s", formatOption(name)))
		}

		parser.named[name] = o
	}
}

func (parser *Parser) set(
	ctx context.Context,
	name string,
	value Value,
) (found bool, err error) {
	var option *internal.Option

	if parser.named != nil {
		option, found = parser.named[name]
	}

	if !found {
		for _, pattern := range parser.patterned {
			if found = pattern.MatchString(name); found {
				option = pattern.Option
				break
			}
		}
	}

	if !found {
		return
	}

	switch {
	case value.Explicit() && option.ValueFunc == nil:
		// Explicit value but no way to use it
		err = fmt.Errorf("option %s doesn’t take a value", formatOption(name))

	case value.Explicit() || option.NoValueFunc == nil:
		// Explicit value or we want one
		var val string
		val, err = value.Get()
		if err == nil {
			err = option.ValueFunc(ctx, name, val)
		}

	case option.NoValueFunc != nil:
		// No explicit value and we don’t want one
		err = option.NoValueFunc(ctx, name)

	default:
		// No value but we wanted one
		err = fmt.Errorf("option %s needs a value", formatOption(name))
	}

	if err != nil {
		err = errör.Composite(ErrInvalidValue, err)
	}

	return
}
