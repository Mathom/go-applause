# 👏 Applause

[![Go Reference](https://pkg.go.dev/badge/gitlab.com/biffen/go-applause.svg)](https://pkg.go.dev/gitlab.com/biffen/go-applause)

Applause is a command-line argument parser for Go.

## Features

-   Short and long options

    `-o` and `--option`.

-   Boolean and value options

    `--bool` or `-b` can set a `bool` to `true`, while `--string value` or
    `-s value` can set a `string` to `"value"`.

-   Bundling

    Short options can be bundled together after a single `-`.

    `-a -b` can be shortened to `-ab`.

-   Implicit or explicit values

    `--string value` and `--string=value` do the same thing. So do `-s value`,
    `-s=value` and even `-svalue`.

-   Struct tags

    Tag struct fields with `option:"<specification>"` and parse options into a
    struct.

-   Parse to ‘anything’

    The built-in parsers can parse a string to any of Go’s basic types (and some
    standard library types). To parse to your own type make it implement
    [`flag.Value`][flag.value]. Or parse to a `func(string)`!

-   Increment, decrement, toggle

    `-vvv` can be made to increment your `verbose` variable three times, and
    then `-qqq` can do the opposite.

    If `--bool` sets your variable to `true`, then you can have `--no-bool` set
    it to `false`.

-   Stop parsing at `--`

    Any argument after `--` is an operand, even if it begins with `-`.

[flag.value]: https://pkg.go.dev/flag#Value
