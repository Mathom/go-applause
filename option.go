package applause

import (
	"regexp"
)

func formatOption(name string) string {
	if len(name) <= 1 {
		return "-" + name
	}
	return "--" + name
}

type Option struct {
	Add      int
	Names    []string
	Negate   bool
	Patterns []*regexp.Regexp
	Target   any
}
