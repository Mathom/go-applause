package applause

import (
	"fmt"
)

var (
	_ Value = (*NoValue)(nil)
	_ Value = (*StringValue)(nil)
	_ Value = (FuncValue)(nil)
)

type FuncValue func() (string, error)

func (f FuncValue) Explicit() bool {
	return false
}

func (f FuncValue) Get() (string, error) {
	return f()
}

type NoValue struct{}

func (NoValue) Explicit() bool {
	return false
}

func (NoValue) Get() (string, error) {
	return "", ErrNoValue
}

type StringValue string

func (s StringValue) Explicit() bool {
	return true
}

func (s StringValue) Get() (string, error) {
	return string(s), nil
}

type Value interface {
	Explicit() bool
	Get() (string, error)
}

func NewValue(value any) Value {
	switch t := value.(type) {
	case string:
		return StringValue(t)

	case func() (string, error):
		return FuncValue(t)

	case nil:
		return NoValue(struct{}{})

	default:
		panic(fmt.Sprintf("bad value type %T", value))
	}
}
