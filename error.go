package applause

const (

	// ErrBadSpec is a bad option specification.
	ErrBadSpec = Error("bad option specification")

	// ErrBadTargetType for when a target is of an unsupported type.
	ErrBadTargetType = Error("bad target type")

	// ErrInvalidValue is for values supplied as arguments that can’t be parsed
	// into their targets.
	ErrInvalidValue = Error("invalid value")

	// ErrMissingTarget for when an option specification appears in a string but
	// no target follows as the next argument.
	ErrMissingTarget = Error("missing target")

	// ErrNilTarget means the target is nil.
	ErrNilTarget = Error("nil target")

	// ErrNoValue for when an option is used but no value is passed.
	ErrNoValue = Error("option requires a value")

	// ErrNotAPointer for when a struct is passed as a value.
	ErrNotAPointer = Error("target struct not a pointer")

	// ErrNotAStruct for when trying to use a struct but the pointer is not to a
	// struct.
	ErrNotAStruct = Error("target not a struct")

	// ErrUnknownOption happens when an unknown option is passed.
	ErrUnknownOption = Error("unknown option")

	// ErrUnsetFunc happens when a `--no-*` option is refers to a function
	// traget.
	ErrUnsetFunc = Error("can’t unset a function")
)

var _ error = Error("")

// Error is an error, for use as constant errors.
type Error string

func (e Error) Error() string {
	return string(e)
}
