package internal_test

import (
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/go-applause/internal"
	"gitlab.com/biffen/typo/pointer"
)

func TestParseName(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		String  string
		Name    *string
		Pattern *regexp.Regexp
	}{
		{"foo", pointer.To("foo"), nil},
		{"/foo/", nil, regexp.MustCompile(`^foo$`)},
	} {
		c := c
		t.Run(c.String, func(t *testing.T) {
			t.Parallel()

			name, pattern := internal.ParseName(c.String)
			assert.Equal(t, c.Name, name)
			if c.Pattern == nil {
				assert.Nil(t, pattern)
			} else {
				require.NotNil(t, pattern)
				assert.Equal(t, c.Pattern.String(), pattern.String())
			}
		})
	}
}
