package internal_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/biffen/go-applause/internal"
)

func TestCamelToKebab(t *testing.T) {
	t.Parallel()

	for _, c := range [...]struct {
		Input, Expected string
	}{
		{"foo", "foo"},
		{"Foo", "foo"},
		{"FooBar", "foo-bar"},
		{"fooBar", "foo-bar"},
		{"FooBARBaz", "foo-bar-baz"},
	} {
		c := c

		t.Run(c.Input, func(t *testing.T) {
			t.Parallel()

			assert.Equal(t, c.Expected, internal.CamelToKebab(c.Input))
		})
	}
}
