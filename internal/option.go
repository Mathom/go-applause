package internal

import (
	"context"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"unicode/utf8"

	"gitlab.com/biffen/typo"
)

const (
	OptionTag = "option"
)

var SpecPattern = regexp.MustCompile(`^([\w|-]*?)([!+-])?$`)

type NoValueFunc = func(ctx context.Context, name string) error

func MakeAddFunc(target reflect.Value, add int) NoValueFunc {
	switch target.Elem().Kind() {
	case reflect.Pointer:
		tmp := reflect.New(target.Type().Elem().Elem())
		f := MakeAddFunc(tmp, add)
		return func(ctx context.Context, str string) error {
			if err := f(ctx, str); err != nil {
				return err
			}
			target.Elem().Set(tmp)
			return nil
		}

	case
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64:
		return func(ctx context.Context, str string) error {
			target.Elem().SetInt(target.Elem().Int() + int64(add))
			return nil
		}

	case
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Uintptr:
		return func(ctx context.Context, str string) error {
			target.Elem().SetUint(target.Elem().Uint() + uint64(add))
			return nil
		}

	case
		reflect.Float32,
		reflect.Float64:
		return func(ctx context.Context, str string) error {
			target.Elem().
				SetFloat(target.Elem().Float() + float64(add))
			return nil
		}

	default:
		panic(
			fmt.Sprintf(
				"can’t add to a %T (%s)",
				target.Interface(),
				target.Kind(),
			),
		)
	}
}

func MakeNoValueFunc(target reflect.Value) NoValueFunc {
	switch target.Elem().Kind() {
	case reflect.Pointer:
		tmp := reflect.New(target.Type().Elem().Elem())
		f := MakeNoValueFunc(tmp)
		if f == nil {
			return nil
		}
		return func(ctx context.Context, str string) error {
			if err := f(ctx, str); err != nil {
				return err
			}
			target.Elem().Set(tmp)
			return nil
		}

	case reflect.Bool:
		return func(ctx context.Context, str string) error {
			target.Elem().SetBool(true)
			return nil
		}

	default:
		return nil
	}
}

type Option struct {
	Add         int
	Names       []string
	Negate      bool
	NoValueFunc NoValueFunc
	Patterns    []*regexp.Regexp
	Target      reflect.Value
	ValueFunc   ValueFunc
}

func OptionsForStruct(s any) (options []Option) {
	typ := reflect.TypeOf(s)

	if typ.Kind() != reflect.Ptr {
		panic(fmt.Sprintf("%s is not a pointer", typ))
	}

	typ = typ.Elem()

	if typ.Kind() != reflect.Struct {
		panic(fmt.Sprintf("%s is not a struct", typ))
	}

	for i := 0; i < typ.NumField(); i++ {
		field := typ.Field(i)
		tag, ok := field.Tag.Lookup(OptionTag)
		if ok && tag == "-" {
			continue
		}

		switch {
		case field.Anonymous:
			options = append(options, OptionsForStruct(
				reflect.ValueOf(s).
					Elem().
					FieldByIndex(field.Index).
					Addr().
					Interface(),
			)...)

			continue

		case ok:
			option := ParseOptionTag(field.Name, tag)

			option.Target = reflect.ValueOf(s).
				Elem().
				FieldByIndex(field.Index).
				Addr()

			options = append(options, option)
		}
	}

	return
}

func ParseOptionTag(fieldName, tag string) (option Option) {
	match := SpecPattern.FindStringSubmatch(tag)

	if len(match) == 0 {
		panic(fmt.Sprintf("bad option tag %q", tag))
	}

	if match[2] != "" {
		flag, _ := utf8.DecodeRuneInString(match[2])
		switch flag {
		case '-':
			option.Add = -1
		case '+':
			option.Add = +1
		case '!':
			option.Negate = true
		default:
			panic("shouldn’t happen")
		}
	}

	names := match[1]

	if names == "" {
		option.Names = []string{CamelToKebab(fieldName)}
	} else {
		for _, n := range strings.Split(names, "|") {
			name, pattern := ParseName(n)
			switch {
			case name != nil:
				option.Names = append(option.Names, *name)
			case pattern != nil:
				option.Patterns = append(option.Patterns, pattern)
			default:
				panic("shouldn’t happen")
			}
		}
	}

	return
}

func (o *Option) Setup() {
	// TODO validate o

	switch t := o.Target.Interface().(type) {
	case ValueFunc:
		o.ValueFunc = t

	case NoValueFunc:
		o.NoValueFunc = t

	default:
		if o.Target.Kind() == reflect.Func {
			panic(fmt.Sprintf("unsupported function target %T", o.Target.Interface()))
		}

		sc := typo.StringConverter{
			SliceAppend: true,
		}
		// TODO separators, etc.

		conv, err := sc.ConvertFunc(t)
		if err != nil {
			panic(err)
		}

		o.ValueFunc = func(ctx context.Context, str, value string) error {
			return conv(ctx, value)
		}

		if o.Add == 0 {
			o.NoValueFunc = MakeNoValueFunc(o.Target)
		} else {
			o.NoValueFunc = MakeAddFunc(o.Target, o.Add)
		}
	}
}

type ValueFunc = func(ctx context.Context, name, value string) error
