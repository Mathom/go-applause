package internal

import (
	"regexp"
	"strings"
)

var camelPattern = regexp.MustCompile(
	`^(\p{Lu}+|(?:^|\p{Lu})\p{Ll}+)(?:\p{Lu}|$)`,
)

// CamelToKebab turns ACamelCaseString into a-kebab-case-string.
func CamelToKebab(str string) string {
	//nolint: prealloc // No way to know.
	var (
		i     int
		parts []string
	)

	for i < len(str) {
		match := camelPattern.FindStringSubmatch(str[i:])

		part := strings.ToLower(match[1])

		i += len(part)

		parts = append(parts, part)
	}

	return strings.Join(parts, "-")
}
