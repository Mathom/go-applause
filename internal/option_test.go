package internal_test

import (
	"context"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/biffen/go-applause/internal"
)

func TestMakeNoValueFunc(t *testing.T) {
	t.Parallel()

	ctx := context.Background()

	t.Run("string", func(t *testing.T) {
		t.Parallel()

		var (
			f internal.NoValueFunc
			s string
		)

		require.NotPanics(t, func() {
			f = internal.MakeNoValueFunc(reflect.ValueOf(&s))
		})
		assert.Nil(t, f)
	})

	t.Run("bool", func(t *testing.T) {
		t.Parallel()

		var (
			b bool
			f internal.NoValueFunc
		)

		require.NotPanics(t, func() {
			f = internal.MakeNoValueFunc(reflect.ValueOf(&b))
		})
		require.NotNil(t, f)

		err := f(ctx, "")
		assert.NoError(t, err)
		assert.True(t, b)
	})

	t.Run("*bool", func(t *testing.T) {
		t.Parallel()

		var (
			b *bool
			f internal.NoValueFunc
		)

		require.NotPanics(t, func() {
			f = internal.MakeNoValueFunc(reflect.ValueOf(&b))
		})
		require.NotNil(t, f)

		err := f(ctx, "")
		assert.NoError(t, err)
		if assert.NotNil(t, b) {
			assert.True(t, *b)
		}
	})
}
