module gitlab.com/biffen/go-applause

go 1.20

require (
	github.com/stretchr/testify v1.8.2
	gitlab.com/biffen/error v0.2.0
	gitlab.com/biffen/typo v0.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/exp v0.0.0-20230321023759-10a507213a29 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
