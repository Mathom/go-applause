# Applause Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog], and this project adheres to [Semantic
Versioning].

## 0.4.0 - 2022-05-01

### Added

-   Slice and map targets

## 0.3.0 - 2021-03-06

### Changed

-   Moved to `gitlab.com/biffen/go-applause`

## 0.2.3 - 2020-12-11

—

## 0.2.2 - 2020-11-24

### Added

-   Increment/decrement options

## 0.1.2 - 2020-10-25

### Added

-   Support for embedded structs

## 0.1.1 - 2020-10-02

### Added

-   Append to slice target

## 0.1.0 - 2020-09-27

### Removed

-   Help text generation

## 0.0.3 - 2020-03-19

### Added

-   Formatting of help text

## 0.0.2 - 2020-03-19

### Added

-   Help text generation

## 0.0.1 - 2020-01-04

Initial implementation.

[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html
